# Stochastic Planning Toolkit

The Stochastic Planning Toolkit used in the experiments for the PhD thesis "Collective Decision Making through Self-regulation: Mechanisms and Algorithms for Self-regulation in Decision-Theoretic Planning" by J. Scharpff (2020).

This project has moved to the [TU Delft Algorithmics group repository](https://github.com/AlgTUDelft/SPTK) on GitHub.
